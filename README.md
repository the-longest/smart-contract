# The longest - Smart contract

## Development

Enure that node with npm is installed on your machine.

To setup your directory run the following:

```shell
npm install
```

Use Visual Studio Code and install the extensions `JuanBlanco.solidity` and `RemixProject.ethereum-remix`.

Please note that you need to open the `smart-contract` folder directly in VS Code and not any parent folder.
Otherwise the compiling not works.

# TODO

- merging on transferring and minting
