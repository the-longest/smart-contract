// SPDX-License-Identifier: GPL-3.0

// solhint-disable-next-line
pragma solidity >=0.4.22 <0.9.0;

import "remix_tests.sol"; 
import "remix_accounts.sol";

import "../contracts/TheLongest.sol";

contract TheLongestTest is TheLongest {
    address internal acc0;
    address internal acc1;
    address internal acc2;

    function beforeAll() public {
        acc0 = TestsAccounts.getAccount(0); 
        acc1 = TestsAccounts.getAccount(1);
        acc2 = TestsAccounts.getAccount(2);
    }

    function oneMintOneToken() public {
        safeMint(acc1);
        uint256 balance = balanceOf(acc1);
        uint256 token = tokenOfOwnerByIndex(acc1, 0);
        uint256 length = getLength(token);
        Assert.equal(balance, 1, "Acc should only have 1 token in wallet");
        Assert.equal(length, 1, "Length should be 1");
    }

    
    function twoMintOneToken() public {
        safeMint(acc2);
        safeMint(acc2);
        uint256 balance = balanceOf(acc2);
        uint256 token = tokenOfOwnerByIndex(acc2, 0);
        uint256 length = getLength(token);
        Assert.equal(balance, 1, "Acc should only have 1 token in wallet");
        Assert.equal(length, 2, "Length should be 2");
    }

    /// #sender: account-2
    function oneMintOneTransferOneToken() public {
        uint256 token2 = tokenOfOwnerByIndex(acc2, 0);
        safeTransferFrom(acc2, acc1, token2);
        uint256 balance = balanceOf(acc1);
        uint256 token1 = tokenOfOwnerByIndex(acc1, 0);
        uint256 length = getLength(token1);
        Assert.equal(balance, 1, "Acc should only have 1 token in wallet");
        Assert.equal(length, 3, "Length should be 3");
    }
}
