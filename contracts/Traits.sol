// SPDX-License-Identifier: MIT

// solhint-disable-next-line
pragma solidity ^0.8.11;

contract Traits {

    // Mapping from trait name to ID
    mapping(string => uint8) private traitMapping;

    // // Mapping from trait (ID) to mapping from tokenID to trait value
    // mapping(uint8 => mapping(uint256 => uint256)) private traitsMapping1;

    // Mapping from tokenID to mapping from trait (ID) to trait value
    mapping(uint256 => mapping(uint8 => uint256)) private traitsMapping2;

    constructor() {
        traitMapping["length"] = 0;
        traitMapping["inverted"] = 1;
    }

    function getTraitFromString(string memory traitName) internal view returns(uint8 id) {
        return traitMapping[traitName];
    }

    function set(uint256 token, uint8 trait, uint256 value) internal {
        traitsMapping2[token][trait] = value;
    }

    function get(uint256 token, uint8 trait) internal view returns(uint256 value) {
        return traitsMapping2[token][trait];
    }

    function remove(uint256 token) internal {
        // delete traitsMapping2[token];
    }
}