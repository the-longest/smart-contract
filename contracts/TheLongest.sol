// SPDX-License-Identifier: MIT

// solhint-disable-next-line
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Royalty.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/utils/Base64.sol";

import "./Traits.sol";

contract TheLongest is ERC721, ERC721Enumerable, ERC721Burnable, ERC721Royalty, Ownable, Traits {

    using Counters for Counters.Counter;

    Counters.Counter private _tokenIdCounter;

    // solhint-disable-next-line func-visibility
    constructor() ERC721("The longest", "LONGEST") {
        totalEth = address(this).balance;
        // royalty of 5 %
        _setDefaultRoyalty(address(this), 500);
    }

    // Mapping from token ID to length of line
    // mapping(uint256 => uint256) private _lengths;

    // the longest
    address public theLongest;
    uint256 public theLongestLength;

    // total ether from royalties
    uint256 public totalEth;

    // withdrawn by the owner
    uint256 public withdrawnByOwner;

    // withdrawn by the longest
    uint256 public withdrawnByTheLongest;

    // ratio to withdraw from the longest (in %)
    uint256 public ratioToWithdrawal = 50;

    uint256 public constant MAX_MINT = 11111;

    uint256 public alreadyMinted = 0;
    uint256 public alreadyBurned = 0;

    // Function to receive Ether. msg.data must be empty
    receive() external payable {
        totalEth += msg.value;
    }

    // Fallback function is called when msg.data is not empty
    fallback() external payable {
        totalEth += msg.value;
    }

    function safeMint(address to) public onlyOwner {
        require(alreadyMinted + 1 <= MAX_MINT, "Max length reached");
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();

        Traits.set(tokenId, 0, 1);
        Traits.set(tokenId, 1, tokenId % 2);

        // _lengths[tokenId] = 1;
        _safeMint(to, tokenId);
        alreadyMinted++;
    }

    function safeMintMass(address[] calldata to) public onlyOwner {
        require(alreadyMinted + to.length <= MAX_MINT, "Max length reached");
        for (uint256 i = 0; i < to.length; i++) {
            safeMint(to[i]);
        }
    }

    // The following functions are overrides to implement the mechanics.

    function _afterTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    )
        internal
        virtual
        override
    {
        super._afterTokenTransfer(from, to, tokenId);
        // dont run on _burn
        if (to != address(0)) {
            // in case no merge happend updating THE longest should happen with 1
            _merge(to, tokenId);
            // update THE longest if wallets change
        } else {
            // to address is here only 0x0 if burn was called
            alreadyBurned++;
        }
    }

    function _merge(address wallet, uint256 tokenAdded) internal {
        uint256 lengthSum = 1;
        // merge if have 2 tokens
        if (balanceOf(wallet) == 2) {
            // TODO: check if index 0 is always the oldest token
            uint256 tokenAlready = tokenOfOwnerByIndex(wallet, 0);
            uint256 lengthAlready = Traits.get(tokenAlready, 0);
            uint256 lengthAdded = Traits.get(tokenAdded, 0);
            // uint256 lengthAlready = _lengths[tokenIdAlready];
            // uint256 lengthNew = _lengths[tokenId];
            lengthSum = lengthAlready + lengthAdded;

            //_lengths[tokenIdAlready] = lengthSum;
            Traits.set(tokenAlready, 0, lengthSum);

            // update inverted
            // inverted is dominant
            uint256 invertedAlredy = Traits.get(tokenAlready, 1);
            uint256 invertedAdded = Traits.get(tokenAdded, 1);

            uint256 invertedNew;
            if (invertedAdded == 1 || invertedAdded == 1) {
                invertedNew = 1;
            } else {
                invertedNew = 0;
            }

            Traits.set(tokenAlready, 1, invertedAlredy);


            _burn(tokenAdded);
            //delete _lengths[tokenId];
        }
        _updateTheLongest(wallet, lengthSum);
    }

    // update the longest
    function _updateTheLongest (
        address to,
        uint256 length
    ) internal {
        if (length > theLongestLength) {
            theLongestLength = length;
            // TODO: what is cheaper? check if changed or write anyway?
            if (theLongest != to) {
                theLongest = to;
            }
        }
    }

    // get length of token
    function getLength(
        uint256 tokenId
    ) public view returns (uint256) {
        //return _lengths[tokenId];
        return Traits.get(tokenId, 0);
    }

    // withdraw from the longest
    function withdrawalTheLongest() external {
        require(msg.sender == theLongest, "You have not the longest");
        if (msg.sender == theLongest) {
            uint256 amount = withdrawalAmountTheLongest();
            withdrawnByTheLongest += amount;
            payable(msg.sender).transfer(amount);
        }
    }

    // withdraw from the owner
    function withdrawalOwner() external onlyOwner {
        uint256 amount = withdrawalAmountOwner();
        withdrawnByOwner += amount;
        payable(msg.sender).transfer(amount);
    }

    // amount to withdraw from the longest
    function withdrawalAmountTheLongest() public view returns (uint256) {
        return (totalEth * ratioToWithdrawal / 100) - withdrawnByTheLongest;
    }

    // amount to withdraw from the owner
    function withdrawalAmountOwner() public view returns (uint256) {
        return (totalEth * (100 - ratioToWithdrawal) / 100) - withdrawnByOwner;
    }

    function _generateSVG(uint256 length, uint256 inverted) internal pure returns (string memory) {
        uint256 maxLength = MAX_MINT;
        uint256 strokeWidth = maxLength / 40;
        string memory colorBackground = "black";
        string memory colorLine = "white";

        uint256 size = maxLength + strokeWidth;
        uint256 center = size / 2;
        uint256 x1 = center - length / 2;
        uint256 x2 = center + length / 2;
        uint256 y1 = center + length / 2;
        uint256 y2 = center - length / 2;
        if (inverted == 1) {
            x1 = center - length / 2;
            x2 = center + length / 2;
            y1 = center - length / 2;
            y2 = center + length / 2;
        }
        string memory rect = string(abi.encodePacked(
            "<rect width='100%' height='100%' fill='",
            colorBackground,
            "' />"
        ));
        string memory strokeWidthString = Strings.toString(strokeWidth);
        string memory line = string(abi.encodePacked(
            "<line x1='",
            Strings.toString(x1),
            "' y1='",
            Strings.toString(y1),
            "' x2='",
            Strings.toString(x2),
            "' y2='",
            Strings.toString(y2),
            "' stroke-linecap='round' stroke='",
            colorLine,
            "' stroke-width='",
            strokeWidthString,
            "' />"
        ));
        return string(abi.encodePacked(
            "<svg version='.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'",
            " id='line'",
            " height='",
            Strings.toString(size),
            "' width='",
            Strings.toString(size),
            "'>",
            rect,
            line,
            "</svg>"
        ));
    }

    function _encodeSVG(string memory svg) internal pure returns (string memory) {
        return string(abi.encodePacked(
            "data:image/svg+xml;base64,",
            Base64.encode(bytes(svg))
        ));
    }

    function tokenURI(uint256 tokenId) public view override returns (string memory) {

        uint256 length = Traits.get(tokenId, 0);
        uint256 inverted = Traits.get(tokenId, 1);

        string memory encodedSVG = _encodeSVG(_generateSVG(length, inverted));

        string memory attributes = string(abi.encodePacked(
            "[",
                "{",
                "\"display_type\": \"number\",",
                "\"trait_type\": \"ID\",",
                "\"value\": ", Strings.toString(tokenId), ",",
                "\"max_value\": ", Strings.toString(MAX_MINT), "",
                "},",
                "{",
                "\"trait_type\": \"Length\",",
                "\"value\": ", Strings.toString(length), ",",
                "\"max_value\": ", Strings.toString(MAX_MINT), "",
                "},",
                "{",
                "\"trait_type\": \"Inverted\",",
                "\"value\": ", Strings.toString(inverted), "",
                "}",
            "]"
        ));

        string memory metadata = string(abi.encodePacked(
            "{\"name\":\"length: ", Strings.toString(length), " #", Strings.toString(tokenId),
            "\",\"description\":\"Who is gonna have the longest.. Your line has length: ",
            Strings.toString(length),
            "\",\"image\": \"",
            encodedSVG,
            "\",\"attributes\":",
            attributes,
            "}"
        ));

        return string(abi.encodePacked(
            "data:application/json;base64,",
            Base64.encode(bytes(metadata))
        ));
    }

    // The following functions are overrides required by ERC721Enumerable and ERC721Royalty.

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable, ERC721Royalty)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function _burn(uint256 tokenId) internal virtual override(ERC721, ERC721Royalty) {
        Traits.remove(tokenId);
        return super._burn(tokenId);
    }

}
